<?php /* Smarty version 3.1.28-dev/63, created on 2015-10-24 06:11:46
         compiled from "C:\xampp\htdocs\smarty\templates\test.tpl" */ ?>
<?php
$_valid = $_smarty_tpl->decodeProperties(array (
  'has_nocache_code' => false,
  'version' => '3.1.28-dev/63',
  'unifunc' => 'content_562b050248eec7_59913875',
  'file_dependency' => 
  array (
    'dceaf03544757a78a2d6b03551c4a53e752968a5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\smarty\\templates\\test.tpl',
      1 => 1445659883,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false);
if ($_valid && !is_callable('content_562b050248eec7_59913875')) {
function content_562b050248eec7_59913875 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_capitalize')) require_once 'C:\\xampp\\htdocs\\smarty\\lib\\plugins\\modifier.capitalize.php';
if (!is_callable('smarty_modifier_russify')) require_once 'C:\\xampp\\htdocs\\smarty\\lib\\plugins\\function.ending.php';
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\xampp\\htdocs\\smarty\\lib\\plugins\\modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) require_once 'C:\\xampp\\htdocs\\smarty\\lib\\plugins\\modifier.truncate.php';
if (!is_callable('smarty_mb_wordwrap')) require_once 'C:\\xampp\\htdocs\\smarty\\lib\\plugins\\shared.mb_wordwrap.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <title>Пример веб-страницы</title>
    </head>
    <body bgcolor="#f5f5dc">
        <?php if (isset($_smarty_tpl->tpl_vars['site_name']->value)) {?>
            <h1><?php echo $_smarty_tpl->tpl_vars['site_name']->value;?>
</h1>
        <?php } else { ?>
            Проверка оператора else
        <?php }?>
        <p>
            <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['text']->value);?>

        </p>
        <p> <?php echo $_smarty_tpl->tpl_vars['item']->value['comments'];?>
 комментар<?php echo smarty_modifier_russify($_smarty_tpl->tpl_vars['item']->value['comments'],'иев','ий','ия');?>
</p>
        <p><?php echo ($_smarty_tpl->tpl_vars['text']->value).(' Это интересно');?>
</p>
        <p><?php echo mb_strlen($_smarty_tpl->tpl_vars['text']->value, 'UTF-8');?>
 - количество символов с пробелами</p>
        <p><?php echo preg_match_all("#\w[\.\?\!](\W|$)#Su", $_smarty_tpl->tpl_vars['text']->value, $tmp);?>
 - количество предложений</p>
        <p><?php echo preg_match_all('/\p{L}[\p{L}\p{Mn}\p{Pd}\'\x{2019}]*/u', $_smarty_tpl->tpl_vars['text']->value, $tmp);?>
 - количество слов</p>
        <p><b><?php echo (($tmp = @$_smarty_tpl->tpl_vars['email']->value)===null||$tmp==='' ? "Not found" : $tmp);?>
</b> - выводим переменную, которая не задана</p>
        <p><?php echo preg_replace('!^!m',str_repeat(' ',4),$_smarty_tpl->tpl_vars['text']->value);?>
 - добавляем отступ</p>
        <p><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['text']->value, 'UTF-8');?>
 - переводим строку в верхний регистр</p>
        <p><?php echo smarty_modifier_date_format(time(),"%A");?>
</p>
        <p><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['text']->value,10,"...",true);?>
 - обрезаем строку</p>
        <p><?php echo smarty_mb_wordwrap($_smarty_tpl->tpl_vars['text']->value,10,"<br/>",false);?>
</p>
        </body>
</html><?php }
}
