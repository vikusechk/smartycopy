<?php
/**
 * Created by PhpStorm.
 * User: Виктория
 * Date: 14.10.2015
 * Time: 22:54
 */
include_once 'lib/Smarty.class.php';

$smarty = new Smarty();
$site_name='Изучаем шаблонизатор Smarty';

$smarty->assign('site_name', $site_name);
$smarty->assign('text', 'Работаем с модификаторами в Smarty.');
$smarty->assign('item.comments', 5);
$smarty->assign('articleTitle', 'Капля никотина убивает лошадь, хомячка разрывает на куски.');

$smarty->register_modifier("pluralF", "plural");

$smarty->force_compile=true;
$smarty->display('test.tpl');


function plural($number, $e0, $e1, $e2)
{
    if ($number % 100 > 10 && $number % 100 < 20) return $e0;
    if ($number % 10 == 1) return $e1;
    if ($number % 10 >= 2 && $number % 10 <= 4) return $e2;
    return $e0;
}

