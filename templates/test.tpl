<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <title>Пример веб-страницы</title>
    </head>
    <body bgcolor="#f5f5dc">
        {if isset($site_name)}
            <h1>{$site_name}</h1>
        {else}
            Проверка оператора else
        {/if}
        <p>
            {$text|capitalize}
        </p>
        <p> {$item.comments} {$item.comments|pluralF:"комментариев":"комментарий":"комментария"}</p>
        <p>{$text|cat:' Это интересно'}</p>
        <p>{$text|count_characters:true} - количество символов с пробелами</p>
        <p>{$text|count_sentences} - количество предложений</p>
        <p>{$text|count_words} - количество слов</p>
        <p><b>{$email|default:"Not found"}</b> - выводим переменную, которая не задана</p>
        <p>{$text|indent} - добавляем отступ</p>
        <p>{$text|upper} - переводим строку в верхний регистр</p>
        <p>{$smarty.now|date_format:"%A"}</p>
        <p>{$text|truncate:10:"...":true} - обрезаем строку</p>
        <p>{$text|wordwrap:10:"<br/>"}</p>
        </body>
</html>